﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        //
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Days * 60 * 24 + (DateTime.Now - DateCreated).Hours * 60 + (DateTime.Now - DateCreated).Minutes;
                //return (DateTime.Now - DateCreated).Minutes;
            }
        }
        //
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}
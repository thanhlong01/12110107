﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Tag
    {
        [Required(ErrorMessage = "Phải nhập nội dung")]
        public int TagID { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự trong khoảng 10 - 100", MinimumLength = 10)]
        public String Content { set; get; }
        //
        public virtual ICollection<Post> Posts { set; get; }
    }
}